<?php
/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 4.8.14
 * Time: 10:34
 */

class Users extends \Phalcon\Mvc\Model  {

	public $id;
	public $nick;
	public $name;
	public $email;
	public $passwd;
	public $salt;
	public $role;

	public function get($id = null){
		$users =  $this::find("id=$id");
		foreach($users as $user){
			$this->id = $user->id;
			$this->nick = $user->nick;
			$this->name = $user->name;
			$this->email = $user->email;
			$this->passwd = $user->passwd;
			$this->salt = $user->salt;
			$this->role = $user->role;

			return $this->toJson();
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('nick','name','email','passwd','salt','role'));
	}
	public function delete($id){
		$user = $this::find("id=$id");
		return	$user->delete();
	}
	public function toJson(){
		return json_encode(array('id'=>$this->id,'nick'=>$this->nick,'name'=>$this->name,'email'=>$this->email,'passwd'=>$this->passwd,
			'salt'=>$this->salt,'role'=>$this->role));
	}