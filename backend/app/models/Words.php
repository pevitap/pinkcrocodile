<?php

class Words extends Phalcon\Mvc\Model
{       
    public $id;
	public $id_name;
	public $img;
	public $video;
	public $sign;
	public $sound;
	public $default;
	public $id_creator;
	public $id_category;

    public function getSource()
    {
        return 'words';
    }   
    
    static function getByCategory($id_category){
		    $word = Words::findFirst(array(
                        "id_category = :id_category:",
                        "bind" => array('id_category' => $id_category)
                    ));         
        return $word;
    }

}