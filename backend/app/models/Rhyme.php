<?php
/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 4.8.14
 * Time: 10:18
 */

class Rhyme extends \Phalcon\Mvc\Model {

	public $id;
	public $id_name;
	public $id_rhyme;
	public $id_category;

	public function get($id = null){
		$rhymes =  $this::find("id=$id");
		foreach($rhymes as $rhyme){
			$this->id = $rhyme->id;
			$this->id_name = $rhyme->id_name;
			$this->id_rhyme = $rhyme->id_rhyme;
			$this->id_category = $this->id_category;
			return $this->toJson();
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('id_name','id_rhyme','id_category'));
	}
	public function delete($id){
		$rhyme = $this::find("id=$id");
		return	$rhyme->delete();
	}
	public function toJson(){
		return json_encode(array('id'=>$this->id,'id_name'=>$this->id_name,'id_rhyme'=>$this->id_rhyme,'id_category'=>$this->id_category));
	}
}