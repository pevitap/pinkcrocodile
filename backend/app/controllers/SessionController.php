<?php

//Author: Marek Doksanský

class SessionController extends Phalcon\Mvc\Controller {

    public function indexAction() {
        
    }

    private function _registerSession($user) {
        $this->session->set('auth', array(
            'id' => $user->id,
            'nick' => $user->nick,
            'username' => $user->name,
            'role' => $user->role
        ));
    }

    public function startAction() {

        if ($this->request->isPost()) {
            
            $receiveData = $this->request->getJsonRawBody();

            $nick = $receiveData->login;
            $password = $receiveData->password;

            $user = Users::getByNick($nick);

            $this->response->setContentType('application/json');
                    
           if($user && crypt($password,$user->salt) == $user->passwd){
                $this->_registerSession($user);
                $this->response->setStatusCode(200, "OK");
                $this->response->setContent(json_encode("Prihlaseni probehlo uspesne."));

            } else {
                $this->response->setStatusCode(403, "Forbidden");
                $this->response->setContent(json_encode("Spatne prihlasovaci jmeno nebo heslo."));
            }

        } else {
           //$this->response->setStatusCode(100, "No send data"); 
           //$this->response->setContent(json_encode("No send data")); 
          
            
        }
        return $this->response;
    }
    
    
    public function LoggedUserRole() {
        $userRole = $this->session->get("auth")["role"];
        $username = $this->session->get("auth")["nick"];

        if ($user = Users::getByNick($username)) {
            $userId = $user->id;
        }

        if ($userRole == "0") {
            $this->response->setStatusCode(200, "OK");
            $responseData = array("role" => "admin", "name" => $username, "id" => $userId);
            $this->response->setContent(json_encode($responseData));
        } else if ($userRole == "1") {
            $this->response->setStatusCode(200, "OK");
            $responseData = array("role" => "user", "name" => $username, "id" => $userId);
            $this->response->setContent(json_encode($responseData));
        } else {
            $this->response->setStatusCode(401, "Unauthorized");
        }

        return $this->response;
    }


    
    public function logoutAction() {
        $this->session->destroy();
    }

}
