/**
 * Created by Vojtěch Havel
 * Date: 8/3/14
 */

    var changeUrl = function(url){
    return "../.."+url;
    }



var app = function (response) {
    console.log(response.responseText);

    var wordDetail= function (myWord) {


        $('.DetailTitle').text(myWord.name.toUpperCase());

        var CatImg = $("<img>");
        CatImg.attr('src', changeUrl(myWord.img) );
        CatImg.addClass("img-responsive");
        CatImg.addClass("col-md-6");
        $('.DetailPicture').append(CatImg);

        //sound
        $(".DetailPicture").addClass('sound');
        $('.DetailPicture').append($("<audio id='DetailAudio'/>"));
        $("#DetailAudio").attr('src',  changeUrl(myWord.sound));
        addSoundOnClick('DetailAudio');

        //main video
        $(".DetailVideo").append($("<video class='video' id='DetVideo' />"));
        $("#DetVideo").attr('src',  changeUrl(myWord.video));


     //sign video
        if(myWord.sign.split(':')[0]=='video'||myWord.sign.split(':')[0]=='yt'){
        $(".DetailSign").append($("<video class='video' id='SignVideo' />"));
        $("#SignVideo").attr('src',  changeUrl(myWord.sign.split(':')[1]));
        }
        else{
            var CatImg = $("<img>");
            $('.DetailSign').append(CatImg);
            CatImg.attr('src', changeUrl(myWord.sign.split(':')[1]) );
            CatImg.addClass("img-responsive");
            CatImg.addClass("col-md-6");
        }


        addClickToPlayToVid();

        var myHover = $("<div>");
        myHover.addClass('Hover');
        $('.DetailPicture').hover(function(){ $(this).append(myHover);},function(){ $(this).children('.Hover').remove();})


    }


    var word= JSON.parse(response.responseText);
    wordDetail(word);


}

var main = function () {
    document.title = getTranslation('DetailTitle');
    //get number of category from url
    var numOfWord = document.URL.split('=')[1];
    //request to list all words in category
    var xhr = new XMLHttpRequest();
    xhr.open('GET', ApiUrl + "/words/" + numOfWord + "?" + lang);
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (typeof cb !== "undefined") {
                cb(this);
            }
            else {
                app(this);
            }
        }
    };
    xhr.send(null);
}
main();
//
//$(document).ready(main);