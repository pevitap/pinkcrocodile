//0 HomeTitle
//1 DecisionLink
//2 TruthDecisionLink
//3 LoginLink
//4 SummaryTitle
//5 DetailTitle
//6 DecisionTitle
//7 TruthDecisionTitle
//8 Search
//9 AdministrationTitle

//10 Cat1
//11 Cat2
//12 Cat3
//13 Cat4

//14 Username
//15 Password
//16 Forgot your password?
//17 Sign in

// en
var eng = new Array('Home','DECISION','YES/NO','LOGIN','Summary','Detail','Decision','Decision YES/NO','Search','Administration','SEASONS','FAMILY','ANIMALS','FOOD','Username','Password','Forgot your password?','Sign in');
// cs
var cze = new Array('Domů','ROZHODOVÁNÍ','ANO/NE','PŘIHLÁŠENÍ','Přehled slov','Detail slova','Rozhodování','Rozhodování ANO/NE','Hledat','Administrace','ROČNÍ OBDOBÍ','RODINA','ZVÍŘATA','JÍDLO','Uživatelské jméno','Heslo','Zapomněli jste heslo?','Přihlásit se');

var getTranslation = function (word) {
    switch (word) {
        case 'HomeTitle':
            switch (lang) {
                case 'en':
		    return eng[0];
		    break;
                case 'cs':
                default:
		    return cze[0];
		    break;
            }
            break;
        case 'DecisionLink':
            switch (lang) {
                case 'en':
                    return eng[1];
                    break;
                case 'cs':
                default:
                    return cze[1];
                    break;
            }
            break;
    	case 'TruthDecisionLink':
            switch (lang) {
                case 'en':
                    return eng[2];
                    break;
                case 'cs':
                default:
                    return cze[2];
                    break;
            }
            break;
    	case 'LoginLink':
            switch (lang) {
                case 'en':
                    return eng[3];
                    break;
                case 'cs':
                default:
                    return cze[3];
                    break;
            }
            break;
        case 'SummaryTitle':
            switch (lang) {
                case 'en':
                    return eng[4];
                    break;
                case 'cs':
                default:
                    return cze[4];
                    break;
            }
            break;
    	case 'DetailTitle':
            switch (lang) {
                case 'en':
                    return eng[5];
                    break;
                case 'cs':
                default:
                    return cze[5];
                    break;
            }
            break;
    	case 'DecisionTitle':
            switch (lang) {
                case 'en':
                    return eng[6];
                    break;
                case 'cs':
                default:
                    return cze[6];
                    break;
            }
            break;
        case 'TruthDecisionTitle':
            switch (lang) {
                case 'en':
                    return eng[7];
                    break;
                case 'cs':
                default:
                    return cze[7];
                    break;
            }
            break;
    	case 'Search':
            switch (lang) {
                case 'en':
                    return eng[8];
                    break;
                case 'cs':
                default:
                    return cze[8];
                    break;
            }
            break;
    	case 'AdministrationTitle':
            switch (lang) {
                case 'en':
                    return eng[9];
                    break;
                case 'cs':
                default:
                    return cze[9];
                    break;
            }
            break;
        case 'Cat1':
            switch (lang) {
                case 'en':
                    return eng[10];
                    break;
                case 'cs':
                default:
                    return cze[10];
                    break;
            }
            break;
        case 'Cat2':
            switch (lang) {
                case 'en':
                    return eng[11];
                    break;
                case 'cs':
                default:
                    return cze[11];
                    break;
            }
            break;
    	case 'Cat3':
            switch (lang) {
                case 'en':
                    return eng[12];
                    break;
                case 'cs':
                default:
                    return cze[12];
                    break;
            }
            break;
    	case 'Cat4':
            switch (lang) {
                case 'en':
                    return eng[13];
                    break;
                case 'cs':
                default:
                    return cze[13];
                    break;
            }
            break;
      case 'Username':
            switch (lang) {
                case 'en':
                    return eng[14];
                    break;
                case 'cs':
                default:
                    return cze[14];
                    break;
            }
            break;
      case 'Password':
            switch (lang) {
                case 'en':
                    return eng[15];
                    break;
                case 'cs':
                default:
                    return cze[15];
                    break;
            }
            break;
      case 'forgot':
            switch (lang) {
                case 'en':
                    return eng[16];
                    break;
                case 'cs':
                default:
                    return cze[16];
                    break;
            }
            break;
      case 'signIn':
            switch (lang) {
                case 'en':
                    return eng[17];
                    break;
                case 'cs':
                default:
                    return cze[17];
                    break;
            }
            break;
       default:
       	       return "Undefined translation";
    }
}